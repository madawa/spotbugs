# spotbugs analyzer changelog

## v2.3.1
- added `--batch-mode` to the default `MAVEN_CLI_OPTS` to reduce the logsize

## v2.3.0
- Add an environment variable `SAST_JAVA_VERSION` to specify which Java version to use (8, 11)
  - Default version remains Java 8
  - Specific versions of Java 8/11 can be set using `JAVA_8_VERSION` and `JAVA_11_VERSION`

## v2.2.3
- Fix report JSON compare keys (`cve`) non-uniqueness by including file path and line information into them 

## v2.2.2
 - Switch primary `mvn`/`mvnw` build method from `compile` to `install`
 - Support builds on cross-referential multi-module projects

## v2.2.1
 - Update sdkman to latest version 5.7.3+337

## v2.2.0
 - Change default behavior to exit early with non-zero exit code if compilation fails
 - Introduce new `FAIL_NEVER` variable, to be set to `1` to ignore compilation failure

## v2.1.0
- Bump Spotbugs to [3.1.12](https://github.com/spotbugs/spotbugs/blob/3.1.12/CHANGELOG.md#3112---2019-02-28)
- Bump Find Sec Bugs to [1.9.0](https://github.com/find-sec-bugs/find-sec-bugs/releases/tag/version-1.9.0)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Merge of the Maven, Gradle, Groovy and SBT analyzers with additional features:
  - Use the SpotBugs CLI for analysis.
  - Report correct source file paths.
  - Handle Maven multi module projects.
  - Only report vulnerabilities in source code files, ignore dependencies' libraries.
  - Add command line parameters and environment variable to set the paths of the Ant, Gradle, Maven, SBT and Java
    executables.
  - Add the `--mavenCliOpts` command line parameter and `MAVEN_CLI_OPTS` environment to pass arguments to Maven.
